'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngSanitize'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', '$sce', function($scope, $sce) {
  $scope.trustSrc = function(src) {
   return $sce.trustAsResourceUrl(src);
  }
  
  $scope.mylocalhost = {src:"http://localhost:4200/"};

}]);
